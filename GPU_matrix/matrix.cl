__kernel void matrix(__global const double* A, __global const double* B, __global double* C, __local double* A_buffer, __local double* B_buffer, __local double* C_buffer, int S, int N)
{
	const size_t g_x = get_global_id(0), g_y = get_global_id(1);
	const size_t x = get_local_id(0), y = get_local_id(1);
	const size_t SA = get_local_size(1), SB = get_local_size(0);
	const size_t SX = get_global_size(0), SY = get_global_size(1);
	
	
	size_t b_x = g_x / SB, b_y = g_y / SA;
	
	//Alapból 0
	C_buffer[y * SB + x] = 0;
	
	for(size_t i = 0; i < N; i++)
	{
		//Töltsük be őket a global memoryból, a megfelelő blokkokat (A és B b-ből)
		event_t betolt[2];
		betolt[0] = async_work_group_copy(A_buffer, A + (b_y * SX * SA + S*SA * i), SA * S, 0);
		betolt[1] = async_work_group_copy(B_buffer, B + (i * SX * SA + b_x * S*SB), S * SB, 0);
		//Várjuk meg amíg betöltődnek
		wait_group_events(2, betolt);
		
		//Szummázuk le számonként (minden work item 1 szám)
		for(size_t j = 0; j < S; j++)
			C_buffer[y * SB + x] += A_buffer[y * S + j] * B_buffer[j * SB + x];
		
		//Nem kezdjük el a következő másolást amíg nincs vége az összegezgetésnek (ez kell ide?)
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	
	//És másoljuk ki az eredményt a global memoryba
	event_t kiir;
	kiir = async_work_group_copy(C + (b_y * SX * SA + SB*SA * b_x), C_buffer, SA*SB, 0);
	wait_group_events(1, &kiir);
}
