#include "CLO.hpp"
#include "header.hpp"
#include <initializer_list>
#include <memory>
#include <algorithm>
#include <exception>
#include <time.h>
#include <fstream>
#include <limits>
#include <chrono>

template<class T>
struct Matrix{
	size_t S, O, S_m, O_m;//Sor Oszlop ; sor-oszlop blokkok száma
	size_t l_s, l_o;//local sor oszlop

	//A számok a megfelelő sorrendben blokkosítva (blokkfolytonosan (?))
	std::unique_ptr<T[]> szamok;

	Matrix(size_t l_o = 16, size_t l_s = 16)//Előre kérjük, hogy mekkora blokkokban tároljuk
	{
		this->l_o = l_o;
		this->l_s = l_s;

		S = 0; O = 0; S_m = 0; O_m = 0;
	}

	void createNew(size_t O, size_t S)
	{
		this->S = S;
		this->O = O;

		//Hány blokkunk van x és y mentén
		S_m = (size_t)std::ceil(1.0 * S / l_s);
		O_m = (size_t)std::ceil(1.0 * O / l_o);

		//A blokkok mérete (hány db számot tartalmaz 1 blokk)
		const size_t block_meret = l_s * l_o;

		//Foglaljuk le a memóriát
		szamok = std::make_unique<T[]>(S_m * O_m * block_meret);
	}

	void readFromFile(const std::string& filename)
	{
		std::ifstream fs(filename.c_str());

		if (!fs)
			throw std::runtime_error("Hiba a fájl \"" + filename + "\" megnyitásakor.");

		try{

			//Olvassuk be a mátrix méretét, mert vagyunk olyan kedvesek, hogy előre megmondjuk
			fs >> O >> S;

			//Hány blokkunk van x és y mentén
			S_m = (size_t)std::ceil(1.0 * S / l_s);
			O_m = (size_t)std::ceil(1.0 * O / l_o);

			//A blokkok mérete (hány db számot tartalmaz 1 blokk)
			const size_t block_meret = l_s * l_o;

			//Foglaljuk le a memóriát
			szamok = std::make_unique<T[]>(S_m * O_m * block_meret);

			//végig blokkonként sorfolytonosan és azokon belül számonként sorfolytonosan, de legalább is így indexeljük
			for (size_t y = 0; y < S; y++)
				for (size_t x = 0; x < O; x++)
				{
					const size_t b_x = x / l_o;
					const size_t b_y = y / l_s;
					
					fs >> szamok[(b_y * O_m + b_x)*block_meret + (y % l_s) * l_o + (x % l_o)];
				}

		}
		catch (...)
		{
			throw std::runtime_error("Hiba a fájl \"" + filename + "\" olvasása közben.");
		}
		fs.close();
	}

	void fastReadFromFile(const std::string filename)
	{
		std::vector<cl_double> vec;

		FILE *f;
		if (fopen_s(&f, filename.c_str(), "rb"))
		{
			throw std::runtime_error("Hiba a fájl \"" + filename + "\" megnyitásakor.");
		}

		fseek(f, 0, SEEK_END);
		long fsize = ftell(f);
		fseek(f, 0, SEEK_SET);

		char *str = new char[fsize + 1], *end;
		fread(str, fsize, 1, f);
		fclose(f);

		str[fsize] = 0;

		
		//Mátrix mérete
		O = std::strtod(str, &end);

		//Ha esetleg itt végeszakadna a fájlnak
		if (str == end)
			throw std::runtime_error("Hiba a fájl \"" + filename + "\" olvasása közben.");
		else
			str = end;

		S = std::strtod(str, &end);
		
		
		//Hány blokkunk van x és y mentén
		S_m = (size_t)std::ceil(1.0 * S / l_s);
		O_m = (size_t)std::ceil(1.0 * O / l_o);

		//A blokkok mérete (hány db számot tartalmaz 1 blokk)
		const size_t block_meret = l_s * l_o;

		//Foglaljuk le a memóriát
		szamok = std::make_unique<T[]>(S_m * O_m * block_meret);

		//végig blokkonként sorfolytonosan és azokon belül számonként sorfolytonosan, de legalább is így indexeljük
		for (size_t y = 0; y < S; y++)
			for (size_t x = 0; x < O; x++)
			{
				const size_t b_x = x / l_o;
				const size_t b_y = y / l_s;
				
				//Van még szám?
				if (str == end)
					throw std::runtime_error("Hiba a fájl \"" + filename + "\" olvasása közben.");
				else
					str = end;

				//És olvassunk egyet
				szamok[(b_y * O_m + b_x)*block_meret + (y % l_s) * l_o + (x % l_o)] = std::strtod(str, &end);
			}
	}

	void kiir()
	{
		//A blokkok mérete (hány db számot tartalmaz)
		const size_t block_meret = l_s * l_o;

		for (size_t y = 0; y < S; y++)
		{
			for (size_t x = 0; x < O; x++)
			{
				const size_t b_x = x / l_o;
				const size_t b_y = y / l_s;

				std::cout << " " << szamok[(b_y * O_m + b_x)*block_meret + (y % l_s) * l_o + (x % l_o)];
			}
			std::cout << std::endl;
		}
	}

	size_t getSize()
	{
		return O_m * S_m * l_o * l_s;
	}
};


int main(int argc, char** args)
{
	//Arg-ok beolvasása, filenevek, wg. sizeok
	std::string fn_A, fn_B;
	size_t SA = 32, SB = 32, S = 32;

	if (argc < 3)
	{
		std::cerr << "Túl kevés argumentum. Add meg a mátrixokat tartalmazó fájlokat.\n";
		return 1;
	}

	fn_A = std::string(args[1]);
	fn_B = std::string(args[2]);

	if (argc > 3)
	{
		SA = atoi(args[3]);
		if (argc > 4)
		{
			SB = atoi(args[4]);
			if (argc > 5)
				S = atoi(args[5]);
		}
	}

	if (SA < 1 || SB < 1  || S < 1)
	{
		std::cerr << "Hibás argumentumok.\n";
		return 1;
	}

	try{
	//OpenCL cuccok inicializálása
	CLO jani = CLO::fromFile("matrix.cl", false);
	
	auto vec = jani.getKernels({"matrix"});
	auto stuff = jani.getStuff();
	
	const cl::CommandQueue& queue = stuff[0].second;
	const cl::Kernel& ker = vec[0];
	const auto& context = jani.getContext();
	
	//Mátrixok megfelelő blokkméretekkel
	Matrix<cl_double> A(S, SA), B(SB, S), C(SB, SA);

	//Olvassuk be a mátrixokat
	A.fastReadFromFile("A.dat");
	B.fastReadFromFile("B.dat");
	C.createNew(B.O, A.S);

	//Csak ha összeszorozhatóak
	if (A.O != B.S)
	{
		std::cerr << "Nem megfelelő mátrixméretek, nem lehet őket összeszorozni" << std::endl;
		return 0;
	}

	auto start = std::chrono::high_resolution_clock::now();

	//Bufferek
	cl::Buffer input_1(context, CL_MEM_READ_ONLY, A.getSize() * sizeof(cl_double)), input_2(context, CL_MEM_READ_ONLY, B.getSize() * sizeof(cl_double)), output(context, CL_MEM_WRITE_ONLY, C.getSize() * sizeof(cl_double));
	cl::LocalSpaceArg A_buff = cl::Local(S * SA * sizeof(cl_double)), B_buff = cl::Local(S * SB * sizeof(cl_double)), C_buff = cl::Local(SA*SB * sizeof(cl_double));

	//Írjuk ki GPU-ra
	queue.enqueueWriteBuffer(input_1, CL_FALSE, 0, A.getSize() * sizeof(cl_double), A.szamok.get());
	queue.enqueueWriteBuffer(input_2, CL_FALSE, 0, B.getSize() * sizeof(cl_double), B.szamok.get());

	//Várjuk meg amíg végez (ha mást is akarnánk csinálni ezzel a queue-val akkor Eventeket kéne bevárni)
	queue.finish();

	//Kernel argumentumok beállítása
	_setArgs(ker, input_1, input_2, output, A_buff, B_buff, C_buff, S, A.O_m);

	//Vége a számolásnak event
	cl::Event ev;
	//Számol
	queue.enqueueNDRangeKernel(ker, 0, cl::NDRange(C.O_m * SB, C.S_m * SA), cl::NDRange(SB, SA), NULL, &ev);

	//És kiolvas amint vége a számolásnak
	auto ev_vec = std::vector<cl::Event>({ ev });
	queue.enqueueReadBuffer(output, CL_TRUE, 0, C.getSize() * sizeof(cl_double), C.szamok.get(), &ev_vec);

	//Nézzük meg az egész GPU-s dolog mennyi CPU oldali időbe telt (ne rondítsunk bele a std out streambe, errorba inkább...)
	std::cerr << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count() << " us" << std::endl;

	//Írjuk ki az eredményt
	C.kiir();

	}
	//Ha valami gond lenne
	catch(cl::Error err)
	{
		std::cerr
		 << "ERROR in main:\n"
         << err.what()
         << "(" << err.err() << ")\n"
		 << error_map[err.err()]
         << std::endl;
	}
	catch(std::runtime_error err)
	{
		std::cerr << "Runtime error:\n" << err.what() << std::endl;
	}
	catch(std::exception e)
	{
		std::cerr << "\n:(\n";
	}
}